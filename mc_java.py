import subprocess
import argparse


def cmd(cmd, shell=True):

    try:
        p = subprocess.Popen(cmd,
                             shell=shell,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)

        out, err = p.communicate()
        ret = p.wait()

        if out:
            print("cmd output: %s" % (out))

        if ret != 0:
            return (False, out, err)
        return (True, out, None)

    except Exception as e:
        return (False, out, e)


def minecraft(docroot):
    cmd_text = ("cd {0} && java -Xms1G -Xmx2G -jar "
                "minecraft_server.jar nogui".format(docroot))
    success, out, err = cmd(cmd=cmd_text)
    if not success:
        if err:
            print("Failed cmd: {0} \n {1}".format(err, out))
            return False
        else:
            print("Failed\nReason: other")
            return False
    else:
        print("Successful cmd: {}".format(out))
        return True


def main():
    parser = argparse.ArgumentParser(description="Create dev instance and"
                                                 "update it")

    # instantiate the sub-functions that web_handler provides
    # arguments for separate sub-functions are mutually exclusive
    parser.add_argument('java',
                        nargs='+',
                        help="java wrapper")
    parser.add_argument('-m', '--minecraft',
                        action='store_true',
                        help="update the site")
    parser.add_argument('-d', '--docroot',
                        nargs='?',
                        default=False,
                        help="minecraft folder location")
    args = parser.parse_args()
    if args.minecraft:
        minecraft(docroot=args.docroot)

if __name__ == '__main__':
    main()
